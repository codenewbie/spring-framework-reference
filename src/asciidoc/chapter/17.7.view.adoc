[[view-velocity]]
=== Velocity & FreeMarker
http://velocity.apache.org[Velocity] and http://www.freemarker.org[FreeMarker] are two
templating languages that can be used as view technologies within Spring MVC
applications. The languages are quite similar and serve similar needs and so are
considered together in this section. For semantic and syntactic differences between the
two languages, see the http://www.freemarker.org[FreeMarker] web site.



[[view-velocity-dependencies]]
==== Dependencies
Your web application will need to include `velocity-1.x.x.jar` or `freemarker-2.x.jar`
in order to work with Velocity or FreeMarker respectively and `commons-collections.jar`
is required for Velocity. Typically they are included in the `WEB-INF/lib` folder where
they are guaranteed to be found by a Java EE server and added to the classpath for your
application. It is of course assumed that you already have the `spring-webmvc.jar` in
your `'WEB-INF/lib'` directory too! If you make use of Spring's 'dateToolAttribute' or
'numberToolAttribute' in your Velocity views, you will also need to include the
`velocity-tools-generic-1.x.jar`



[[view-velocity-contextconfig]]
==== Context configuration
A suitable configuration is initialized by adding the relevant configurer bean
definition to your `'*-servlet.xml'` as shown below:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<!--
	This bean sets up the Velocity environment for us based on a root path for templates.
	Optionally, a properties file can be specified for more control over the Velocity
	environment, but the defaults are pretty sane for file based template loading.
	-->
	<bean id="velocityConfig" class="org.springframework.web.servlet.view.velocity.VelocityConfigurer">
		<property name="resourceLoaderPath" value="/WEB-INF/velocity/"/>
	</bean>

	<!--
	View resolvers can also be configured with ResourceBundles or XML files. If you need
	different view resolving based on Locale, you have to use the resource bundle resolver.
	-->
	<bean id="viewResolver" class="org.springframework.web.servlet.view.velocity.VelocityViewResolver">
		<property name="cache" value="true"/>
		<property name="prefix" value=""/>
		<property name="suffix" value=".vm"/>
	</bean>
----

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<!-- freemarker config -->
	<bean id="freemarkerConfig" class="org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer">
		<property name="templateLoaderPath" value="/WEB-INF/freemarker/"/>
	</bean>

	<!--
	View resolvers can also be configured with ResourceBundles or XML files. If you need
	different view resolving based on Locale, you have to use the resource bundle resolver.
	-->
	<bean id="viewResolver" class="org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver">
		<property name="cache" value="true"/>
		<property name="prefix" value=""/>
		<property name="suffix" value=".ftl"/>
	</bean>
----

[NOTE]
====
For non web-apps add a `VelocityConfigurationFactoryBean` or a
`FreeMarkerConfigurationFactoryBean` to your application context definition file.
====



