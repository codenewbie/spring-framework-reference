[[beans-factory-scopes-request]]
===== Request scope
Consider the following bean definition:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="loginAction" class="com.foo.LoginAction" scope="request"/>
----

The Spring container creates a new instance of the `LoginAction` bean by using the
`loginAction` bean definition for each and every HTTP request. That is, the
`loginAction` bean is scoped at the HTTP request level. You can change the internal
state of the instance that is created as much as you want, because other instances
created from the same `loginAction` bean definition will not see these changes in state;
they are particular to an individual request. When the request completes processing, the
bean that is scoped to the request is discarded.


[[beans-factory-scopes-session]]
===== Session scope
Consider the following bean definition:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="userPreferences" class="com.foo.UserPreferences" scope="session"/>
----

The Spring container creates a new instance of the `UserPreferences` bean by using the
`userPreferences` bean definition for the lifetime of a single HTTP `Session`. In other
words, the `userPreferences` bean is effectively scoped at the HTTP `Session` level. As
with `request-scoped` beans, you can change the internal state of the instance that is
created as much as you want, knowing that other HTTP `Session` instances that are also
using instances created from the same `userPreferences` bean definition do not see these
changes in state, because they are particular to an individual HTTP `Session`. When the
HTTP `Session` is eventually discarded, the bean that is scoped to that particular HTTP
`Session` is also discarded.


[[beans-factory-scopes-global-session]]
===== Global session scope
Consider the following bean definition:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="userPreferences" class="com.foo.UserPreferences" scope="globalSession"/>
----

The `global session` scope is similar to the standard HTTP `Session` scope
(<<beans-factory-scopes-session,described above>>), and applies only in the context of
portlet-based web applications. The portlet specification defines the notion of a global
`Session` that is shared among all portlets that make up a single portlet web
application. Beans defined at the `global session` scope are scoped (or bound) to the
lifetime of the global portlet `Session`.

If you write a standard Servlet-based web application and you define one or more beans
as having `global session` scope, the standard HTTP `Session` scope is used, and no
error is raised.


[[beans-factory-scopes-application]]
===== Application scope
Consider the following bean definition:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<bean id="appPreferences" class="com.foo.AppPreferences" scope="application"/>
----

The Spring container creates a new instance of the `AppPreferences` bean by using the
`appPreferences` bean definition once for the entire web application. That is, the
`appPreferences` bean is scoped at the `ServletContext` level, stored as a regular
`ServletContext` attribute. This is somewhat similar to a Spring singleton bean but
differs in two important ways: It is a singleton per `ServletContext`, not per Spring
'ApplicationContext' (or which there may be several in any given web application),
and it is actually exposed and therefore visible as a `ServletContext` attribute.


